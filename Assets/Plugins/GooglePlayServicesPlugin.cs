﻿using UnityEngine;

public static class GooglePlayServicesPlugin
{
    private static AndroidJavaClass _class;

    public static void Initialize()
    {
        if(!Application.isEditor && Application.platform == RuntimePlatform.Android)
        {
            _class = new AndroidJavaClass("com.deepvigil.gpg.GooglePlayGamesPlugin");
            _class.CallStatic("Initialize");
        }
    }

    public static string Conenct()
    {
        if (!Application.isEditor && Application.platform == RuntimePlatform.Android)
        {
            return _class.CallStatic<string>("Conenct");
        }

        return "NotAndroidPlatform";
    }

    public static void Disconnect()
    {
        if (!Application.isEditor && Application.platform == RuntimePlatform.Android)
        {
            _class.CallStatic<string>("Disconnect");
        }
    }
}
